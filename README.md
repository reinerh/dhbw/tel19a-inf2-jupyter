[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/reinerh%2Fdhbw%2Ftel19a-inf2-jupyter/master?urlpath=lab)

# TEL19A-Inf2-Jupyter

Repo2Docker-Konfiguration für das Repository TEL19A-Inf2 zur Verwendung mit BinderHub o.Ä.

Enthält keine Materialien, nur eine konfigurierte Jupyter-Umgebung mit C++-Kernel, Compiler etc. sowie ein Startskript, das die Materialien herunterlädt.